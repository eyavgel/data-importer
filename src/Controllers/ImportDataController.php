<?php

namespace smartdevpro\import_data\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use smartdevpro\import_data\Classes\Import;

class ImportDataController extends Controller
{
    /**
     * Show page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('import_data::import');
    }

    /**
     * @param Request $request
     */
    public function process(Request $request)
    {
        $path = $request->file('file')->store('file');

        $import = new Import();
        $import->convert($path);
        $import->process();
    }
}
