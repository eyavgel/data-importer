<?php

Route::get('import', 'smartdevpro\import_data\Controllers\ImportDataController@show');
Route::post('import', 'smartdevpro\import_data\Controllers\ImportDataController@process');
