<?php

namespace smartdevpro\import_data\Classes;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class Import
{
    protected $data;
    protected $tableName;
    protected $title;

    /**
     * Import constructor.
     *
     * @param null $path
     */
    public function __construct($path = null)
    {
        $this->convert($path);
        $this->tableName = 'table1';
    }

    /**
     * Convert from source to json
     *
     * @param $path
     * @return array|void
     */
    public function convert($path)
    {
        if (!$path) return;

        $contents = Storage::get($path);

        $rows = explode("\n", $contents);

        foreach ($rows as $key => $row){
            $rows[$key] = explode(',', $row);
        }

        foreach ($rows[0] as $key => $name){
            $rows[0][$key] = str_replace(' ', '_', strtolower(trim($name, ' "')));
        }

        $this->title = $rows[0];
        $titleLength = count($this->title);

        array_shift($rows);

        $data = [];
        foreach ($rows as $key => $row) {
            if ($titleLength !== count($row)) continue;

            foreach ($row as $index => $column) {
                $data[$key][$this->title[$index]] = trim($column, ' "');
            }
        }

        return $this->data = $data;
    }

    /**
     * Set table name
     *
     * @param $tableName
     */
    public function setDestination($tableName): void
    {
        $this->tableName = $tableName;
    }

    /**
     * Process
     */
    public function process()
    {
        if ($this->checkTable()) {
            $this->clearTable();
        } else {
            $this->createTable();
        }

        $this->insertToTable();
        $this->createLogFile();
    }

    /**
     * Check table
     *
     * @return bool
     */
    private function checkTable()
    {
        if (Schema::hasTable($this->tableName)) {
            $columns = Schema::getColumnListing($this->tableName);

            if(count(array_diff($columns, array_merge($this->title, ['id', 'created_at', 'updated_at']))) === 0) {
                return true;
            }

            $this->dropTable();
        }

        return false;
    }

    /**
     *  Create table
     */
    private function createTable(): void
    {
        $title = $this->title;
        Schema::create($this->tableName, static function (Blueprint $table) use ($title) {
            $table->increments('id');
            foreach ($title as $item) {
                $table->string($item);
            }
            $table->timestamps();
        });
    }

    /**
     *  Clear table
     */
    private function clearTable(): void
    {
        DB::table($this->tableName)->truncate();
    }

    /**
     * Drop table
     */
    private function dropTable(): void
    {
        Schema::drop($this->tableName);
    }

    /**
     * Insert to table
     */
    private function insertToTable(): void
    {
        $insert_data = collect($this->data); // Make a collection to use the chunk method

        // it will chunk the dataset in smaller collections containing 500 values each.
        // Play with the value to get best result
        $chunks = $insert_data->chunk(2000);

        foreach ($chunks as $chunk)
        {
            \DB::table($this->tableName)->insert($chunk->toArray());
        }
    }

    private function createLogFile(): void
    {
        Storage::put('importData/' . $this->tableName . '.json', json_encode($this->data));
    }
}
