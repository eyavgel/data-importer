<?php

namespace smartdevpro\import_data;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;

class ImportDataServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function register(): void
    {
        // register package controller
        $this->app->make('smartdevpro\import_data\Controllers\ImportDataController');

        // register package views
        $this->loadViewsFrom(__DIR__.'/views', 'import_data');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        include __DIR__.'/routes/routes.php';
    }
}
