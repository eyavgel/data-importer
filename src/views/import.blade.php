<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Data Importer</title>
    </head>
    <body>
        <form action="/import" enctype="multipart/form-data" method="POST">
            <p>
                <label for="file">
                    <input type="file" name="file" id="file">
                </label>
            </p>
            <button type="submit">Import</button>
        </form>
    </body>
</html>
